#include "ns3/core-module.h"
#include <iostream>
using namespace ns3;
NS_LOG_COMPONENT_DEFINE("HELLO_SIMULATOR");
static void printhello(std::string Name,std::string Number)
{
    std::cout<<Simulator::Now()<<"Hello "<<Name<<" "<<Number<<std::endl;
    Simulator::Schedule(Seconds(1),&printhello,Name,Number);
}

int main(int argc,char * argv[])
{
    CommandLine cmd;
    std::string name,number;
    int n;
    cmd.AddValue("name","scanf your name",name);
    cmd.AddValue("number","scanf your number",number);
    cmd.AddValue("count","scanf print count",n);
    cmd.Parse(argc,argv);
    printhello(name,number);
    Simulator::Stop(Seconds(n));
    Simulator::Run();
    Simulator::Destroy();
    return 0;
}