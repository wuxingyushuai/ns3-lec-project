尝试自己在TCP和IP之间添加一个IPSec协议子层

## 添加一个新模块

在顶层目录中执行以下命令，要创建的新模块名称为my_ipsec

`$ ./utils/create-module.py my_ipsec`

之后会在`contrib`目录下新建一个`my_ipsec`文件夹，文件夹目录如下：

![Alt text](../image/content.png)


## 自定义协议类

打开`my_ipsec.h`文件

添加代码：

```
#ifndef MY_IPSEC_H
#define MY_IPSEC_H
// Add a doxygen group for this module.
// If you have more than one file, this should be in only one of them.
/**
 * \defgroup my_ipsec Description of the my_ipsec
 */
#include "ns3/ipv4-l3-protocol.h"
namespace ns3 {
												/*	声明一个命名空间ns3，所有NS-3的类和函数都在这个命名空间中定义，以避免命名冲突。	*/
class MyIpSecProtocol : public Ipv4L3Protocol	/*	继承自Ipv43Protocol	*/
{
public:
  static TypeId GetTypeId (void);/*声明一个静态方法GetTypeId，返回类型的唯一标识符TypeId。TypeId是NS-3中用于运行时类型识别和对象系统的一部分*/
  MyIpSecProtocol ();							/*声明一个默认构造函数，用于初始化MyIpSecProtocol对象*/
  virtual ~MyIpSecProtocol ();					/*声明一个虚析构函数，以确保当删除一个指向派生类对象的基类指针时，能够正确调用派生类的析构函数。*/

  virtual void Send (Ptr<Packet> packet, Ipv4Address source, Ipv4Address destination, uint8_t protocol, Ptr<Ipv4Route> route);
  virtual void Receive(Ptr<NetDevice> device, Ptr<const Packet> p, uint16_t protocol, const Address &from,
                          const Address &to, NetDevice::PacketType packetType);
    /*声明一个虚函数Send，用于发送数据包。参数包括要发送的数据包、源地址、目的地址、协议号和路由信息。*/
    /*声明一个虚函数Receive，用于接收数据包。参数包括接收的数据包、IP头信息和输入接口。	*/
private:
  void Encrypt ();	/*声明一个私有方法Encrypt，用于对数据包进行加密处理。参数是指向数据包的指针。*/
  void Decrypt ();	/*声明一个私有方法Decrypt，用于对数据包进行解密处理。参数是指向数据包的指针*/
};

} // namespace ns3


#endif /* MY_IPSEC_H */

```

打开`my_ipsec.cc`文件，添加以下代码：

```
#include "my_ipsec.h"
#include "ns3/simulator.h"
#include "ns3/log.h"
namespace ns3 {

NS_LOG_COMPONENT_DEFINE ("MyIpSecProtocol");		//定义日志组件名称
NS_OBJECT_ENSURE_REGISTERED (MyIpSecProtocol);		//确保MyIpSecProtocol类在NS3对象系统中注册的宏

TypeId
MyIpSecProtocol::GetTypeId (void)
{
  static TypeId tid = TypeId ("ns3::MyIpSecProtocol")	//创建一个类型ID，并命名为"ns3::MyIpSecProtocol"
    .SetParent<Ipv4L3Protocol> ()						//类继承自Ipv4L3Protocol
    .AddConstructor<MyIpSecProtocol> ();				//添加构造函数，使得NS-3的对象系统可以动态创建该类的实例。
  return tid;											//返回类型ID
}

MyIpSecProtocol::MyIpSecProtocol () {}
MyIpSecProtocol::~MyIpSecProtocol () {}

void
MyIpSecProtocol::Send (Ptr<Packet> packet, Ipv4Address source, Ipv4Address destination, uint8_t protocol, Ptr<Ipv4Route> route)
{
  NS_LOG_INFO ("Encrypting packet");					//日志打印信息：加密数据包ing
  Encrypt ();										//加密
  Ipv4L3Protocol::Send (packet, source, destination, protocol, route);//使用基类IPv4Protocol的send方法发送数据包
}

void
MyIpSecProtocol ::Receive(Ptr<NetDevice> device, Ptr<const Packet> p, uint16_t protocol, const Address &from,
                          const Address &to, NetDevice::PacketType packetType) {
        NS_LOG_FUNCTION(this);
        Decrypt();
        Ipv4L3Protocol::Receive(device,p,protocol,from,to,packetType);
    }

void
MyIpSecProtocol::Encrypt ()
{
  // IpsHeader ipsHeader;				//IpsHeader ipsHeader 创建一个IpsHeader对象，用于存储IPSec头信息
  // ipsHeader.SetSecurityParameters (1234, Simulator::Now ().GetMilliSeconds ());
  //   //设置安全参数，例如安全参数索引（SPI）为1234，序列号为当前仿真时间的毫秒数。
  // packet->AddHeader (ipsHeader);	//IPSec头信息添加到数据包中
  NS_LOG_INFO ("Encrypting packet");
}

void
MyIpSecProtocol::Decrypt ()
{
  // IpsHeader ipsHeader;				//存储从数据包中移除的IPSec头信息。
  // packet->RemoveHeader (ipsHeader);	//从数据包中移除IPSec头信息，并将其存储到ipsHeader对象中。
  // NS_LOG_INFO ("Received IPSec packet with SPI=" << ipsHeader.GetSpi () << " and Seq=" << ipsHeader.GetSeq ());//日志记SPI和序列号
  NS_LOG_DEBUG("Decrypting packet");
}

} // namespace ns3

```

- 自定义协议类MyIpSecProtocol，继承自Ipv4L3Protocol。
- 通过NS-3的对象系统注册了这个协议。

## 修改install函数

找到`ns-allinone-3.42/ns-3.42/src/internet/helper/internet-stack-helper.cc`文件，修改install代码，安装自定义的协议，而非Ipv4L3Protocol协议，修改代码如下：
```
if (m_ipv4Enabled)
    {
        /* IPv4 stack */
        CreateAndAggregateObjectFromTypeId(node, "ns3::ArpL3Protocol");
        //CreateAndAggregateObjectFromTypeId(node, "ns3::Ipv4L3Protocol");
        CreateAndAggregateObjectFromTypeId(node, "ns3::MyIpSecProtocol");
        CreateAndAggregateObjectFromTypeId(node, "ns3::Icmpv4L4Protocol");
        if (!m_ipv4ArpJitterEnabled)
        {
            Ptr<ArpL3Protocol> arp = node->GetObject<ArpL3Protocol>();
            NS_ASSERT(arp);
            arp->SetAttribute("RequestJitter",
                              StringValue("ns3::ConstantRandomVariable[Constant=0.0]"));
        }
```
## 添加需要的依赖库

在`CMakeLists.txt`文件中添加所需依赖的库

```
build_lib(
    LIBNAME my_ipsec
    SOURCE_FILES model/my_ipsec.cc
                 helper/my_ipsec-helper.cc
    HEADER_FILES model/my_ipsec.h
                 helper/my_ipsec-helper.h
    
    LIBRARIES_TO_LINK
        ${libinternet}
        ${libmobility}
        ${libaodv}
    TEST_SOURCES test/my_ipsec-test-suite.cc
                 ${examples_as_tests_sources}
)
```

## 编译运行

在顶层文件中，运行如下命令

``
./ns3 configure
``

`` ./ns3 build``

若没有报错，则创建成功

## 在scratch中添加测试代码,测试新模块。

```
#include "ns3/core-module.h"           // 包含NS-3核心模块的定义
#include "ns3/network-module.h"        // 包含网络模块的定义
#include "ns3/internet-module.h"       // 包含互联网模块的定义
#include "ns3/my_ipsec-helper.h"       // 包含自定义IPSec辅助类的定义
#include "ns3/point-to-point-module.h" // 包含点对点链路模块的定义
#include "ns3/packet-sink-helper.h"    // 包含数据包接收器的辅助类定义

using namespace ns3; // 使用NS-3命名空间
NS_LOG_COMPONENT_DEFINE("MyApp");
class MyApp : public Application 
{
public:
  MyApp (); // 构造函数
  virtual ~MyApp (); // 析构函数

  // 设置应用程序的参数
  void Setup (Ptr<Socket> socket, Address address, uint32_t packetSize, uint32_t nPackets, DataRate dataRate);

private:
  virtual void StartApplication (void); // 启动应用程序
  virtual void StopApplication (void);  // 停止应用程序

  void ScheduleTx (void); // 安排数据包发送
  void SendPacket (void); // 发送数据包

  Ptr<Socket>     m_socket; // 发送套接字
  Address         m_peer;   // 目标地址
  uint32_t        m_packetSize; // 数据包大小
  uint32_t        m_nPackets; // 数据包数量
  DataRate        m_dataRate; // 数据传输速率
  EventId         m_sendEvent; // 发送事件的ID
  bool            m_running; // 应用程序是否正在运行的标志
  uint32_t        m_packetsSent; // 已发送的数据包数量
};

MyApp::MyApp ()
  : m_socket (0), 
    m_peer (), 
    m_packetSize (0), 
    m_nPackets (0), 
    m_dataRate (0), 
    m_sendEvent (), 
    m_running (false), 
    m_packetsSent (0)
{
}

MyApp::~MyApp()
{
  m_socket = 0; // 释放套接字资源
}

void 
MyApp::Setup (Ptr<Socket> socket, Address address, uint32_t packetSize, uint32_t nPackets, DataRate dataRate)
{
  m_socket = socket; // 设置套接字
  m_peer = address; // 设置目标地址
  m_packetSize = packetSize; // 设置数据包大小
  m_nPackets = nPackets; // 设置数据包数量
  m_dataRate = dataRate; // 设置数据传输速率
}

void 
MyApp::StartApplication (void)
{
  m_running = true; // 标记应用程序为运行状态
  m_packetsSent = 0; // 初始化已发送的数据包数量
  m_socket->Bind (); // 绑定套接字
  m_socket->Connect (m_peer); // 连接到目标地址
  SendPacket (); // 发送第一个数据包
}

void 
MyApp::StopApplication (void)
{
  m_running = false; // 标记应用程序为停止状态

  if (m_sendEvent.IsPending ())
    {
      Simulator::Cancel (m_sendEvent); // 取消未发送的事件
    }

  if (m_socket)
    {
      m_socket->Close (); // 关闭套接字
    }
}

void 
MyApp::SendPacket (void)
{
  Ptr<Packet> packet = Create<Packet> (m_packetSize); // 创建数据包
  m_socket->Send (packet); // 发送数据包

  // 如果还未发送完所有数据包，调度下一个发送
  if (++m_packetsSent < m_nPackets)
    {
      ScheduleTx (); // 安排下一个发送
    }
}

void 
MyApp::ScheduleTx (void)
{
  if (m_running) // 如果应用程序正在运行
    {
      Time tNext (Seconds (m_packetSize * 8 / static_cast<double> (m_dataRate.GetBitRate ()))); // 计算下一个发送时间
      m_sendEvent = Simulator::Schedule (tNext, &MyApp::SendPacket, this); // 安排发送事件
    }
}

int 
main (int argc, char *argv[])
{

  // 启用日志
  LogComponentEnable("PacketSink", LOG_LEVEL_INFO);
  LogComponentEnable("MyApp", LOG_LEVEL_INFO);
  NodeContainer nodes; // 创建节点容器
  nodes.Create (2); // 创建两个节点

  // 在所有节点上安装基本互联网栈
  InternetStackHelper internet;
  internet.Install (nodes);

  MyIpSecHelper ipsecHelper; // 创建自定义IPSec辅助对象
  ipsecHelper.Install (nodes); // 在节点上安装IPSec

  PointToPointHelper pointToPoint; // 创建点对点链路助手
  pointToPoint.SetDeviceAttribute ("DataRate", StringValue ("5Mbps")); // 设置设备数据速率
  pointToPoint.SetChannelAttribute ("Delay", StringValue ("2ms")); // 设置链路延迟

  NetDeviceContainer devices; // 创建网络设备容器
  devices = pointToPoint.Install (nodes); // 安装网络设备

  Ipv4AddressHelper address; // 创建IPv4地址助手
  address.SetBase ("10.1.1.0", "255.255.255.0"); // 设置网络地址

  Ipv4InterfaceContainer interfaces = address.Assign (devices); // 分配地址给设备

  uint16_t sinkPort = 8080; // 定义接收端口
  Address sinkAddress (InetSocketAddress (interfaces.GetAddress (1), sinkPort)); // 创建接收地址
  PacketSinkHelper packetSinkHelper ("ns3::TcpSocketFactory", sinkAddress); // 创建数据包接收器助手
  ApplicationContainer sinkApps = packetSinkHelper.Install (nodes.Get (1)); // 在接收节点上安装接收器应用
  sinkApps.Start (Seconds (1.)); // 设置接收器应用启动时间
  sinkApps.Stop (Seconds (10.)); // 设置接收器应用停止时间

  Ptr<Socket> ns3TcpSocket = Socket::CreateSocket (nodes.Get (0), TcpSocketFactory::GetTypeId ()); // 创建TCP套接字
  Ptr<MyApp> app = CreateObject<MyApp> (); // 创建应用程序实例
  app->Setup (ns3TcpSocket, sinkAddress, 1040, 1000, DataRate ("1Mbps")); // 设置应用程序参数
  nodes.Get (0)->AddApplication (app); // 在发送节点上添加应用程序
  app->SetStartTime (Seconds (2.)); // 设置应用程序启动时间
  app->SetStopTime (Seconds (10.)); // 设置应用程序停止时间

  Simulator::Stop (Seconds (10.)); // 设置模拟器停止时间
  Simulator::Run (); // 运行模拟器
  Simulator::Destroy (); // 销毁模拟器
  return 0; // 返回0，表示程序正常结束
}

}
```


