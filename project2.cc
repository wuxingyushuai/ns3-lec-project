

// Target Network Topology
//
//   Wifi 10.1.3.0
//                 AP
//  *    *    *    *
//  |    |    |    |    10.1.1.0
// n5   n6   n7   n0 -------------- n1   n2   n3   n4
//                   point-to-point  |    |    |    |
//                                   ================
//                                     Wifi 10.1.3.0

#include "ns3/core-module.h"
#include <iostream>
#include "ns3/applications-module.h"
#include "ns3/internet-module.h"
#include "ns3/mobility-module.h"
#include "ns3/network-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/ssid.h"
#include "ns3/yans-wifi-helper.h"

using namespace ns3;

NS_LOG_COMPONENT_DEFINE("Project2-simple_Topology");

int
main(int argc,char * argv[])
{
    bool verbose = true;
    uint32_t n1Wifi = 3;
    uint32_t n2Wifi = 3;
    bool tracing =false;

    CommandLine cmd(__FILE__);
    cmd.AddValue("n1Wifi","Number of Wifi1 STA devices",n1Wifi);
    cmd.AddValue("n2Wifi","Number of Wifi2 STA devices",n2Wifi);
    cmd.AddValue("verbose","Tell echo applications to log if true", verbose);
    cmd.AddValue("tracing", "Enable pcap tracing", tracing);
    if (n1Wifi>250 ||n2Wifi>250)
    {
        std::cout<<"Number of Wifi STA is too much"<<std::endl;
        return 1;
    }
    if (verbose)
    {
        LogComponentEnable("UdpEchoClientApplication", LOG_LEVEL_INFO);
        LogComponentEnable("UdpEchoServerApplication", LOG_LEVEL_INFO);
    }
    
    //构造P2P节点 n0 n1
    NodeContainer p2pNodes;
    p2pNodes.Create(2);

    PointToPointHelper pointToPoint;
    pointToPoint.SetDeviceAttribute("DataRate",StringValue("5Mbps"));
    pointToPoint.SetChannelAttribute("Delay", StringValue("2ms"));

    NetDeviceContainer p2pDevices;
    p2pDevices = pointToPoint.Install(p2pNodes);

    //构造左边的WiFi节点
    NodeContainer Wifi1staNodes;
    Wifi1staNodes.Create(n1Wifi);

    NodeContainer Wifi1ApNode;
    Wifi1ApNode = p2pNodes.Get(0);
    YansWifiChannelHelper channel1 = YansWifiChannelHelper::Default();
    YansWifiPhyHelper phy;
    phy.SetChannel(channel1.Create());

    WifiMacHelper mac1;
    Ssid ssid1 = Ssid("ns3-ssid1");

    WifiHelper wifi1;

    NetDeviceContainer sta1Devices;
    mac1.SetType("ns3::StaWifiMac", "Ssid", SsidValue(ssid1), "ActiveProbing", BooleanValue(false));
    sta1Devices = wifi1.Install(phy,mac1,Wifi1staNodes);

    NetDeviceContainer Ap1Device;
    mac1.SetType("ns3::ApWifiMac", "Ssid", SsidValue(ssid1));
    Ap1Device = wifi1.Install(phy,mac1,Wifi1ApNode);
    //构造右边的WiFi节点
    NodeContainer Wifi2staNodes;
    Wifi2staNodes.Create(n2Wifi);

    NodeContainer Wifi2ApNode;
    Wifi2ApNode = p2pNodes.Get(1);
    YansWifiChannelHelper channel2 = YansWifiChannelHelper::Default();
    YansWifiPhyHelper phy2;
    phy2.SetChannel(channel2.Create());

    WifiMacHelper mac2;
    Ssid ssid2 = Ssid("ns3-ssid2");

    WifiHelper wifi2;

    NetDeviceContainer sta2Devices;
    mac2.SetType("ns3::StaWifiMac", "Ssid", SsidValue(ssid2), "ActiveProbing", BooleanValue(false));
    sta2Devices = wifi2.Install(phy2,mac2,Wifi2staNodes);

    NetDeviceContainer Ap2Device;
    mac2.SetType("ns3::ApWifiMac", "Ssid", SsidValue(ssid2));
    Ap2Device = wifi2.Install(phy2,mac2,Wifi2ApNode);

    MobilityHelper mobility;
    mobility.SetPositionAllocator("ns3::GridPositionAllocator",
                                  "MinX",
                                  DoubleValue(0.0),
                                  "MinY",
                                  DoubleValue(0.0),
                                  "DeltaX",
                                  DoubleValue(5.0),
                                  "DeltaY",
                                  DoubleValue(10.0),
                                  "GridWidth",
                                  UintegerValue(3),
                                  "LayoutType",
                                  StringValue("RowFirst"));
    
    //将两个AP节点设为固定模式
    mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
    mobility.Install(Wifi1ApNode);
    mobility.Install(Wifi2ApNode);
    //将左边的AP节点设置为随机移动模型
    mobility.SetMobilityModel("ns3::RandomWalk2dMobilityModel",
                              "Bounds",
                              RectangleValue(Rectangle(-50, 50, -50, 50)));
    mobility.Install(Wifi1staNodes);

    //将右边的Ap节点设置为线性远离AP
    mobility.SetMobilityModel("ns3::ConstantVelocityMobilityModel");
    mobility.Install(Wifi2staNodes);
    for (uint32_t i = 0; i < Wifi2staNodes.GetN(); ++i) 
        {
            Ptr<ConstantVelocityMobilityModel> cvMob = Wifi2staNodes.Get(i)->GetObject<ConstantVelocityMobilityModel>();
            cvMob->SetVelocity(Vector(5.0, 0.0, 0.0));  // 速度为 5 m/s，沿 X 轴正方向移动
        }

    //设置协议栈
    InternetStackHelper stack;
    stack.Install(Wifi1ApNode);
    stack.Install(Wifi1staNodes);
    stack.Install(Wifi2staNodes);
    stack.Install(Wifi2ApNode);
    Ipv4AddressHelper address;
    address.SetBase("10.1.1.0", "255.255.255.0");
    Ipv4InterfaceContainer p2pInterfaces;
    p2pInterfaces = address.Assign(p2pDevices);

    Ipv4InterfaceContainer ap1Interfaces;

    address.SetBase("10.1.2.0", "255.255.255.0");
    address.Assign(sta1Devices);
    ap1Interfaces=address.Assign(Ap1Device);

    address.SetBase("10.1.3.0", "255.255.255.0");
    address.Assign(sta2Devices);
    address.Assign(Ap2Device);

    //设置应用程序，完成4个client与1个server的通信
    // 设置 Server 应用（在 AP1 上）
    UdpEchoServerHelper echoServer(9);
    ApplicationContainer serverApps = echoServer.Install(Wifi1ApNode.Get(0));  // AP1 作为服务器
    serverApps.Start(Seconds(1.0));
    serverApps.Stop(Seconds(10.0));

    // 设置 4 个 Client 应用
    // Client 1（与服务器在同一子网）
    UdpEchoClientHelper echoClient1(ap1Interfaces.GetAddress(0), 9);  // 连接到 AP1
    echoClient1.SetAttribute("MaxPackets", UintegerValue(1));
    echoClient1.SetAttribute("Interval", TimeValue(Seconds(1.0)));
    echoClient1.SetAttribute("PacketSize", UintegerValue(1024));
    ApplicationContainer clientApps1 = echoClient1.Install(Wifi1staNodes.Get(0));  // 同子网中的一个 STA 节点
    clientApps1.Start(Seconds(2.0));
    clientApps1.Stop(Seconds(10.0));
    // Client 2（从 AP2 子网连接服务器）
    UdpEchoClientHelper echoClient2(ap1Interfaces.GetAddress(0), 9);  // 连接到 AP1
    echoClient2.SetAttribute("MaxPackets", UintegerValue(1));
    echoClient2.SetAttribute("Interval", TimeValue(Seconds(1.0)));
    echoClient2.SetAttribute("PacketSize", UintegerValue(1024));
    ApplicationContainer clientApps2 = echoClient2.Install(Wifi2staNodes.Get(0));  // AP2 子网中的 STA 节点
    clientApps2.Start(Seconds(2.0));
    clientApps2.Stop(Seconds(10.0));

    // Client 3（从 AP2 子网连接服务器）
    UdpEchoClientHelper echoClient3(ap1Interfaces.GetAddress(0), 9);  // 连接到 AP1
    echoClient3.SetAttribute("MaxPackets", UintegerValue(1));
    echoClient3.SetAttribute("Interval", TimeValue(Seconds(1.0)));
    echoClient3.SetAttribute("PacketSize", UintegerValue(1024));
    ApplicationContainer clientApps3 = echoClient3.Install(Wifi2staNodes.Get(1));  // AP2 子网中的另一个 STA 节点
    clientApps3.Start(Seconds(2.0));
    clientApps3.Stop(Seconds(10.0));

    // Client 4（从 AP2 子网连接服务器）
    UdpEchoClientHelper echoClient4(ap1Interfaces.GetAddress(0), 9);  // 连接到 AP1
    echoClient4.SetAttribute("MaxPackets", UintegerValue(1));
    echoClient4.SetAttribute("Interval", TimeValue(Seconds(1.0)));
    echoClient4.SetAttribute("PacketSize", UintegerValue(1024));
    ApplicationContainer clientApps4 = echoClient4.Install(Wifi2ApNode);  // AP2 子网中的第三个 STA 节点
    clientApps4.Start(Seconds(2.0));
    clientApps4.Stop(Seconds(10.0));
    Ipv4GlobalRoutingHelper::PopulateRoutingTables();

    Simulator::Stop(Seconds(10.0));

    // if (tracing)
    // {
    //     phy.SetPcapDataLinkType(WifiPhyHelper::DLT_IEEE802_11_RADIO);
    //     pointToPoint.EnablePcapAll("third");
    //     phy.EnablePcap("third", apDevices.Get(0));
    //     csma.EnablePcap("third", csmaDevices.Get(0), true);
    // }

    Simulator::Run();
    Simulator::Destroy();

    return 0;
}